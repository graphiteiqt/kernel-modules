ccflags-y += -I$(src) -I$(ANDROID_BUILD_TOP)/kernel/modules/include -DINTREE_BUILD=1

obj-$(CONFIG_SS_MOD) += ss_mod_core.o ss_netlink_connector.o ss_policyact.o ss_spaceinfo.o ss_springboard.o

CFLAGS_REMOVE_trace_persistent.o = -pg
