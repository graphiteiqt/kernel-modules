/*
 * 2013-2014. Graphite Software Corporation.
 */

#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/string.h>
#include <linux/slab.h>
#include <linux/sched.h>
#include <linux/signal.h>
#include "ss_mod.h"
#include "ss_spaceinfo.h"
#include "ss_policyact.h"

// -----------------------------------------------------------------
// list of adbd procs

struct proc_struct {
  struct list_head hook;
  struct pid* p;                    // key
};

static struct list_head* proc_list; 

static void add_proc(struct pid *p) {
  struct proc_struct *procp = kmalloc(sizeof(struct proc_struct),GFP_KERNEL);
  procp->p = p;
  list_add(&procp->hook,proc_list);
}

static void delete_proc(struct proc_struct *procp) {
  list_del(&procp->hook);
  kfree(procp);
}

static void destroy_proc_list(void) {
  struct proc_struct* procp, *proc_aux;
  list_for_each_entry_safe(procp, proc_aux, proc_list, hook) {
    delete_proc(procp);
  }
  kfree(proc_list);
}

static void init_proc_list(void) {
  proc_list = kmalloc(sizeof(struct list_head),GFP_KERNEL);
  INIT_LIST_HEAD(proc_list);
}

static void freeze_proc(void) {
  struct task_struct *p;
  PRINTK(KERN_INFO "ss_mod: freeze_proc\n");
  rcu_read_lock();
  for_each_process(p) {
    if(strstr(p->comm,"adbd")) {
      PRINTK(KERN_INFO "ss_mod: freezing:'%s' pid:%d\n", p->comm, task_tgid_vnr(p));
      add_proc(task_pid(p));
      kill_pid(task_pid(p), SIGSTOP, 1);
    }
  }
  rcu_read_unlock();
}

static void unfreeze_proc(void) {
  struct proc_struct* pp;
  rcu_read_lock();
  list_for_each_entry(pp,proc_list,hook) {
    struct pid* p = pp->p;
    if(p != 0) {
      PRINTK(KERN_INFO "ss_mod: unfreezing\n");
      kill_pid(p, SIGCONT, 1);
    }
  }

  destroy_proc_list();
  init_proc_list();

  rcu_read_unlock();
}

static void kill_all_adbd_procs(void) {
  struct task_struct *p;
  struct proc_struct* procp, *proc_aux;
  rcu_read_lock();
  for_each_process(p) {
    if(strstr(p->comm,"adbd")) {
      PRINTK(KERN_INFO "ss_mod: killing:'%s' pid:%d\n", p->comm, task_tgid_vnr(p));
      kill_pid(task_pid(p), SIGKILL, 1);
    }
  }
  list_for_each_entry_safe(procp, proc_aux, proc_list, hook) {
    delete_proc(procp);
  }
  rcu_read_unlock();
}

static struct space_struct* CURRENT_FOREGROUND_SPACE=0;
static int CURRENT_BLOCK_ALL_DEBUG=0;

// Apply policies for a foreground space
//
void apply_foreground_policies(struct space_struct* spacep) {
  if(!spacep) {
    PRINTK(KERN_ERR "ss_mod:Space not found.\n");
    return;
  }
  CURRENT_FOREGROUND_SPACE=spacep;

  if(CHECK_POLICY(spacep,ANTIDEBUG)) {
    CURRENT_BLOCK_ALL_DEBUG=1;
    freeze_proc();
  }
  else {
    CURRENT_BLOCK_ALL_DEBUG=0;
    unfreeze_proc();
  }
}

void apply_device_wide_policy(void) {
  if(CHECK_DEVICE_POLICY(ANTIDEBUG)) {
    kill_all_adbd_procs();
  }
  else {
    // adbd should restart on its own
  }
}

int check_current_should_block_debug(void) {
  return CURRENT_BLOCK_ALL_DEBUG;
}

// startup
//
void ss_policy_init(void) {
  init_proc_list();
}

// shutdown
//
void ss_policy_exit(void) {
  destroy_proc_list();
}

