/*
 * 2013-2014. Graphite Software Corporation.
 */

#ifndef __SS_MOD_H
#define __SS_MOD_H

asmlinkage long ss_mod_sys_kill(int pid, int sig);

#ifdef SS_MOD_QUIET
#define PRINTK(...) 
#else
#define PRINTK(...)  printk(__VA_ARGS__)
#endif

#endif
