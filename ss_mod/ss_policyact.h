/*
 * 2013-2014. Graphite Software Corporation.
 */

#ifndef __SS_POLICYACT_H
#define __SS_POLICYACT_H

#include <linux/kernel.h>
#include <linux/slab.h>
#include <linux/list.h>
#include "ss_mod.h"

struct space_struct;

void apply_foreground_policies(struct space_struct* spacep);
void apply_device_wide_policy(void);
int check_current_should_block_debug(void);

void ss_policy_init(void);
void ss_policy_exit(void);

#endif
