ARCH=arm
SUBARCH=arm
CROSS_COMPILE=arm-eabi-

# LOCAL_MFMODE :=
# LOCAL_MFMODE := -fmf_analysis 
# LOCAL_MFMODE := -fmf_protection
# CROSS_COMPILE=mf
# CFLAGS_MODULE=$(LOCAL_MFMODE) -fmf_debug=11 -fmf_target_env=linux_gnu_arm -fmf_compiler=/Volumes/android/android-build/prebuilts/gcc/darwin-x86/arm/arm-eabi-4.7/bin/arm-eabi-gcc
# LDFLAGS_MODULE=$(LOCAL_MFMODE) -fmf_debug=11 -fmf_target_env=linux_gnu_arm -fmf_compiler=/Volumes/android/android-build/prebuilts/gcc/darwin-x86/arm/arm-eabi-4.7/bin/arm-eabi-gcc

TARGET = libss_mod

DEBUGFLAG=-D_DEBUGTRACE

INCDIR ?= $(KERNEL_DIR)/../modules/include/
CFLAGS_MODULE+=-I$(INCDIR) $(DEBUGFLAG)
INCLUDES = $(INCDIR)/ss_magic.h
libss_mod-objs = ss_mod_core.o ss_netlink_connector.o ss_springboard.o ss_spaceinfo.o ss_policyact.o

obj-m += $(TARGET).o

MAP_FILE ?= $(KERNEL_DIR)/System.map
PARSE_TOOL = parseSystemMap
SYM_OUTHEADER = auto_symbol_header.h
KERNEL_MARKER = .kernel.marker

# LAST_KERNEL_DIR carries what is in the file pointed to by KERNEL_MARKER
# or an empty string
ifeq ($(wildcard $(KERNEL_MARKER)),)
LAST_KERNEL_DIR =
else
LAST_KERNEL_DIR = $(shell cat $(KERNEL_MARKER))
endif

.PHONY: all
all: kernel_marker echo_kdir $(SYM_OUTHEADER) $(INCLUDES)
	$(MAKE) -C $(KERNEL_DIR) M=$(CURDIR) ARCH=arm CROSS_COMPILE=$(CROSS_COMPILE) modules

.PHONY: echo_kdir
echo_kdir:
	@echo "===>"
	@echo "===> Building against KERNEL_DIR: '$(KERNEL_DIR)'"
	@echo "===>"

# checks if the env variable KERNEL_DIR is pointing to different kernel than last time
# - stashes away KERNEL_DIR in the KERNEL_MARKER file
# - this MUST be a phony target, or else the change of dir check never happens
.PHONY: kernel_marker
kernel_marker:
ifneq ($(LAST_KERNEL_DIR),$(KERNEL_DIR))
	@echo $(KERNEL_DIR) > $(KERNEL_MARKER)
	@rm -f $(SYM_OUTHEADER)
	$(MAKE) $(SYM_OUTHEADER)
endif

# for race conditions - .kernel.marker doesn't exist yet
$(KERNEL_MARKER): kernel_marker

$(SYM_OUTHEADER): $(MAP_FILE) $(PARSE_TOOL) $(KERNEL_MARKER)
	./$(PARSE_TOOL) $(MAP_FILE) $(SYM_OUTHEADER)

.PHONY: clean
clean:
	$(MAKE) -C $(LAST_KERNEL_DIR) M=$(PWD) clean
	rm -f $(SYM_OUTHEADER)
	rm -f $(KERNEL_MARKER)
