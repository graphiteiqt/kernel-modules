/*
 * 2013-2014. Graphite Software Corporation.
 */

#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/types.h>
#include <linux/connector.h>
#include "ss_magic.h"
#include "ss_spaceinfo.h"
#include "ss_policyact.h"


static unsigned int msg_seq=0;
static struct cb_id cn_id_netlink_connector;

unsigned int cn_idx = CN_IDX_DRBD;
module_param(cn_idx, uint, 0444);
unsigned int cn_val = 0;
module_param(cn_val, uint, 0444);

static int netlink_connector_send(void * v, size_t is) {
  char *buffer = kmalloc(sizeof(struct cn_msg)+is, GFP_KERNEL);
  struct cn_msg *cn_reply = (struct cn_msg *) buffer;
  int rc;

  if (buffer == NULL) {
    pr_err("ss_mod: netlink_connector out of memory.\n");
    return -ENOMEM;
  }
  memset(buffer, 0, sizeof(struct cn_msg)+is);
  cn_reply->id.idx    = cn_id_netlink_connector.idx;
  cn_reply->id.val    = cn_id_netlink_connector.val;
  cn_reply->seq       = msg_seq++;
  cn_reply->ack       = 0;
  cn_reply->len       = is;
  cn_reply->flags     = 0;
  memcpy(cn_reply->data, v, is);

  rc = cn_netlink_send(cn_reply, cn_idx, GFP_NOIO);
  if (rc && rc != -ESRCH) {
    pr_err("ss_mod: netlink_connector: cn_netlink_send()=%d\n", rc);
  }
    
  return rc;
}

// Process netlink messages
static void netlink_connector_callback(struct cn_msg *msg, struct netlink_skb_parms *params) {
  __u16 flags = msg->flags;
  char* data;
  struct ll* x;
  struct space_struct* spacep;
  int myintdata=0, myintdata2=0;
  
  switch(flags) {
  case SS_CMD_MSG:
    data = (char *)msg->data;
    pr_info("ss_mod: netlink_connector callback message '%s'\n",data);
    break;
  case SS_CMD_SPACE_ID:
    x = (struct ll*) msg->data;
    pr_info("ss_mod: netlink_connector register space id: 0x%x, 0x%x\n",x->int1,x->int2);
    spacep = new_space(x->int1,x->int2); // (uid,space_type);  
    break;
  case SS_CMD_FOREGROUND_SPACE:
    x = (struct ll*) msg->data;
    pr_info("ss_mod:received foreground space: %d\n",x->int1);
    spacep = find_space(x->int1); // uid
    if(spacep != 0) {
      apply_foreground_policies(spacep);
    }
    break;
  case SS_CMD_UNREGISTER_SPACE:
    x = (struct ll*) msg->data;
    pr_info("ss_mod:received unregister space: %d\n",x->int1);
    spacep = find_space(x->int1);
    if(spacep != 0) {
      delete_space(spacep);
    }
    break;
  case SS_CMD_PUSH_DEBUG_POLICY:
    x = (struct ll*) msg->data;
    myintdata = x->int1;
    myintdata2 = x->int2;
    pr_info("ss_mod:received push debug policy: %d %d\n",x->int1, x->int2);

    // TODO - rewrite more efficiently - e.g. conditional bit-setting
    // TODO - remove magic values
    if(myintdata2 == SS_PUSH_ALL_ANTIDEBUG) {
      if(myintdata == SS_DEVICEWIDE) {
	SET_DEVICE_POLICY(ANTIDEBUG);  // just sets the parameter
	apply_device_wide_policy();    // applies the kill
	// sendup_message_info("kern: pushed device-wide antidebug",info);
      }
      else {
	spacep = find_space(myintdata);
	/* if(!spacep) { */
	/*   sendup_message_info("kern: error pushing antidebug to space",info); */
	/*   return -1; */
	/* } */
	if(spacep) {
	  SET_POLICY(spacep,ANTIDEBUG);
	  // sendup_message_info("kern: pushed antidebug to space",info);
	}
      }
    }
    else if(myintdata2 == SS_PUSH_ALL_NOANTIDEBUG) {
      if(myintdata == SS_DEVICEWIDE) {
	UNSET_DEVICE_POLICY(ANTIDEBUG);
	apply_device_wide_policy();
	// sendup_message_info("kern: pushed device-wide antidebug off",info);
      }
      else {
	spacep = find_space(myintdata);
	  /* if(!spacep) { */
	  /*   sendup_message_info("kern: error pushing antidebug off to space",info); */
	  /*   return -1; */
	  /* } */
	if(spacep) {
	  UNSET_POLICY(spacep,ANTIDEBUG);
	  // sendup_message_info("kern: pushed antidebug off to space",info);
	}
      }
    }
    break;
  }
}

// Initialize netlink sockets
// int __init netlink_connector_init(void) {
int netlink_connector_init(void) {
  int err, try=100;

  cn_id_netlink_connector.idx = cn_idx;
  cn_id_netlink_connector.val = ++cn_val;
  do {
    err = cn_add_callback(&cn_id_netlink_connector, "cn_netlink_connector", &netlink_connector_callback);
    if (!err)
      break;
    cn_id_netlink_connector.val = ++cn_val;
  } while (try--);

  if (err) {
    pr_err("ss_mod: netlink_connector: cn_netlink_connector failed to register\n");
    return err;
  }
    
  pr_info("ss_mod: netlink_connector started on %d:%d\n", cn_idx, cn_val);

  netlink_connector_send("ss_mod nelink connector started.",33);
  
  return 0;
}

// Release netlink sockets
// void __exit netlink_connector_exit(void) {
void netlink_connector_exit(void) {
  cn_del_callback(&cn_id_netlink_connector);
  pr_info("ss_mod: netlink_connector exited");
}
