/*
 * 2013-2014. Graphite Software Corporation.
 */

#ifndef __SS_SPACEINFO_H
#define __SS_SPACEINFO_H

#include <linux/list.h>

extern unsigned int DEVICE_WIDE_FLAG;

struct space_struct {
  struct list_head hook;
  int uid;                    // key
  int space_type;             // Work, Open, Personal
  unsigned int policy_flags;  // policy info
};

#define ANTIDEBUG    0x00000001
#define IP_BLACKLIST 0x00000010
#define SET_POLICY(sp,flag) (sp->policy_flags |= flag)
#define UNSET_POLICY(sp,flag) (sp->policy_flags &= ~flag)
#define CHECK_POLICY(sp,flag) (sp->policy_flags & flag)
#define ENFORCE_ALL_POLICIES(sp) (sp->policy_flags = 0xFFFFFFFF)

#define SET_DEVICE_POLICY(flag) (DEVICE_WIDE_FLAG |= flag)
#define UNSET_DEVICE_POLICY(flag) (DEVICE_WIDE_FLAG &= ~flag)
#define CHECK_DEVICE_POLICY(flag) (DEVICE_WIDE_FLAG & flag)

// creates a new space and adds it to the list
struct space_struct* new_space(int uid, int space_type);

// void add_space(struct space_struct *spacep);
struct space_struct *find_space(int uid);
void delete_space(struct space_struct *spacep);
int does_space_exist(int uid);
void dump_space_info(struct space_struct *sp);
void dump_all_space_info(void);

int ss_spaceinfo_init(void);
void ss_spaceinfo_exit(void);


#endif
