/*
 * 2013-2014. Graphite Software Corporation.
 */

#ifndef __SS_NETLINK_CONNECTOR
#define __SS_NETLINK_CONNECTOR

int netlink_connector_init(void);
void netlink_connector_exit(void);

#endif
