/*
 * 2013-2014. Graphite Software Corporation.
 */

#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/string.h>
#include <linux/slab.h>
#include "ss_mod.h"
#include "ss_policyact.h"
#include "ss_spaceinfo.h"

unsigned int DEVICE_WIDE_FLAG = 0;

// -----------------------------------------------------------------
// list of existing Spaces and associated dynamic info
static struct list_head* spaces_list;

static void add_space(struct space_struct *spacep) {
  list_add(&spacep->hook,spaces_list);
}

struct space_struct *find_space(int uid) {
  struct space_struct *sp = 0;
  struct space_struct *spacep = 0;

  list_for_each_entry(sp,spaces_list,hook) {
    if(sp->uid == uid) {
      spacep = sp;
      break;
    }
  }

  if(spacep) { PRINTK(KERN_INFO "ss_mod:foundspace: %d\n",spacep->uid); }
  //  else { PRINTK(KERN_INFO "ss_mod: space not found!"); }

  return spacep;
}

int does_space_exist(int uid) {
  struct space_struct* sp = 0;
  sp = find_space(uid);
  return (sp != 0);
}

void delete_space(struct space_struct *spacep) {
  list_del(&spacep->hook);
  kfree(spacep);
}

// creates a new space and adds it to the list
struct space_struct* new_space(int uid, int space_type) {
  struct space_struct* spacep;
  spacep = kmalloc(sizeof(struct space_struct),GFP_KERNEL);
  spacep->uid = uid;
  spacep->space_type = space_type;
  add_space(spacep);
  ENFORCE_ALL_POLICIES(spacep);
  return spacep;
}


void dump_space_info(struct space_struct *sp) {
  PRINTK(KERN_INFO "ss_mod:SPACE uid:%d type:%d",sp->uid,sp->space_type);
  PRINTK(KERN_INFO "ss_mod:      policy_flags:%d",sp->policy_flags);
  if(CHECK_POLICY(sp,ANTIDEBUG)) { PRINTK(KERN_INFO "ss_mod:                   ANTIDEBUG set"); }
  else { PRINTK(KERN_INFO "ss_mod:                   ANTIDEBUG not set"); }
}

// debugging only
void dump_all_space_info(void) {
  struct space_struct* sp = 0;
  list_for_each_entry(sp,spaces_list,hook) {
    dump_space_info(sp);
  }
}


// initialization
int ss_spaceinfo_init(void) {
  spaces_list = kmalloc(sizeof(struct list_head),GFP_KERNEL);
  INIT_LIST_HEAD(spaces_list);

  ss_policy_init();

  return 0;
}

// tear-down
void ss_spaceinfo_exit(void) {
  struct space_struct* spacep = 0, *aux = 0;
  /* struct proc_struct* procp = 0, *proc_aux = 0; */

  // free lists
  list_for_each_entry_safe(spacep, aux, spaces_list, hook) {
    delete_space(spacep);
  }
  kfree(spaces_list);

  ss_policy_exit();
}
