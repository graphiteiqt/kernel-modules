/*
 * 2013-2014. Graphite Software Corporation.
 */

#include <linux/module.h>
#include <linux/kernel.h>
#include <asm/unistd.h>
#include <linux/syscalls.h>
#include <linux/types.h>
#include <asm/fcntl.h>
#include <asm/errno.h>
#include <linux/types.h>
#include <linux/dirent.h>
#include <linux/string.h>
#include <linux/fs.h>
#include <linux/ptrace.h>
#include <linux/sched.h>
#include <linux/pid.h>
#include <linux/cred.h>
#include <linux/socket.h>
#include <linux/un.h>
#include <asm/uaccess.h>
#include "ss_spaceinfo.h"
#include "ss_policyact.h"
#include "ss_netlink_connector.h"
#include "ss_mod.h"

MODULE_LICENSE("GPL");

// -------------------------------------------------------------------------------------------------------
// Secure Space Module
// - hooks in the kernel
// -------------------------------------------------------------------------------------------------------

//
// The file: auto_symbol_header.h is produced by the perl script parseSystemMap by parsing System.map in the kernel build.
// It produces a set of declarations like this:
//
// const unsigned long ADDR_sys_call_table = 0xc000e0a8;
// const unsigned long ADDR_sys_ptrace = 0xc007744c;
// const unsigned long ADDR_sys_write = 0xc0127d0c;
//
#ifndef INTREE_BUILD
#include "auto_symbol_header.h"
#endif

static unsigned long* sys_call_table = 0;

/* Search method for sys_call_table

Looking at arch/arm/kernel/entry-common.S we'll see that sys_call_table
address is loaded into vector_swi and into sys_syscall procedures.

﻿ENTRY(vector_swi)
        sub     sp, sp, #S_FRAME_SIZE
        stmia   sp, {r0 - r12}          @ Calling r0 - r12
        add     r8, sp, #S_PC
        stmdb   r8, {sp, lr}^           @ Calling sp, lr
        mrs     r8, spsr                @ called from non-FIQ mode, so ok.
        str     lr, [sp, #S_PC]         @ Save calling PC
        str     r8, [sp, #S_PSR]        @ Save CPSR
        str     r0, [sp, #S_OLD_R0]     @ Save OLD_R0
        zero_fp
        [... more stuff here ...]
        ﻿get_thread_info tsk
        adr     tbl, sys_call_table     @ load syscall table pointer
        ldr     ip, [tsk, #TI_FLAGS]    @ check for syscall tracing                                        
        [... more stuff here ...]

sys_syscall:
        bic    scno, r0, #__NR_OABI_SYSCALL_BASE
        cmp    scno, #__NR_syscall - __NR_SYSCALL_BASE
        cmpne    scno, #NR_syscalls    @ check range
        stmloia    sp, {r5, r6}        @ shuffle args
        movlo    r0, r1
        movlo    r1, r2
        movlo    r2, r3
        movlo    r3, r4
        ldrlo    pc, [tbl, scno, lsl #2]    
            ; tbl  = sys_call_table
            ; scno = syscall number
        b    sys_ni_syscall
ENDPROC(sys_syscall)

0k, vector_swi is called by SWI instruction (Software Interrupt), and 
interrupt calls addresses are defined in the vector table.

RTFM-ing ARM specifications, we can know fixed vector table addresses,
vector_swi is at 0x00000008 or 0xffff0008. 

Exception Type    Normal Address    High Vector Address
--------------    --------------    -------------------
Reset             0x00000000        0xFFFF0000
Undefined Inst.   0x00000004        0xFFFF0004
Software Inter.   0x00000008        0xFFFF0008
Prefetch Abort    0x0000000C        0xFFFF000C
Data Abort        0x00000010        0xFFFF0010
IRQ               0x00000018        0xFFFF0018
FIQ               0x0000001C        0xFFFF001C

In short, we'll need to do:
    Read 0xffff0008: LDR PC, vector_swi and get vector_swi offset.
    Search within vector_swi, the adr tbl, sys_call_table instruction.
    Extract the sys_call_table address.

*/

static unsigned long* search_swi(void) {
  const void *vSWI_LDR_addr = (void*) 0xFFFF0008;
  unsigned long* ptr = NULL;
  unsigned long vSWI_offset = 0;
  unsigned long vSWI_instruction = 0;
  unsigned long *vt_vSWI;
  unsigned long sct_offset = 0;
  unsigned long *sys_call_table_addr = 0;

  int isAddr = 0;

  /* vSWI_instruction = vSWI_offset = sct_offset = 0; */

  memcpy(&vSWI_instruction, vSWI_LDR_addr, sizeof(vSWI_instruction));
  vSWI_offset = vSWI_instruction & (unsigned long)0x00000fff; 
  vt_vSWI = (unsigned long *) ((unsigned long)vSWI_LDR_addr+vSWI_offset+8);
  ptr = (unsigned long *) *vt_vSWI;

  /* isAddr = 0; */
  while (!isAddr) {
    isAddr = ( ((*ptr) & ((unsigned long)0xffff0000)) == 0xe28f0000 );
    if (isAddr) {
      sct_offset = (*ptr) & ((unsigned long)0x00000fff);
      sys_call_table_addr = (unsigned long *) ((unsigned long)ptr + 8 + sct_offset);

      // PRINTK(KERN_ERR "ss_mod: Found sys_call_table_addr = 0x%x...\n",(unsigned int) sys_call_table_addr);

      return sys_call_table_addr;

      break;
    }
    ptr++;
  }

  return NULL;
}

#ifndef INTREE_BUILD

#define VERIFY_SYS_ADDRESS(N,A) \
  if(sys_call_table[N] != A) { \
    PRINTK(KERN_INFO "ss_mod: " #N  " does not match " #A " :0x%lx != 0x%lx\n",sys_call_table[N],A);	\
    rc++; \
  }

//
// Verify assumptions about where syscall table is and the syscalls in which we are interested
//
static int verify_sys_call_table_address(const unsigned long* sys_call_table_from_map) {
  int rc = 0;
  unsigned long* sc_tab_searched = 0;
  sc_tab_searched = search_swi();

  if(sc_tab_searched != sys_call_table_from_map) {
    PRINTK(KERN_INFO "ss_mod: System.map sys_call_table_addr=0x%x does not match searched address=0x%x...\n",
	   (unsigned int) sys_call_table_from_map, (unsigned int) sc_tab_searched  );
    rc++;
  }

  // system-call table is ok
  sys_call_table = (unsigned long*) sc_tab_searched;

  // verify assumptions about addresses in system-call table
  VERIFY_SYS_ADDRESS(__NR_ptrace,ADDR_sys_ptrace);
  VERIFY_SYS_ADDRESS(__NR_connect,ADDR_sys_connect);
  VERIFY_SYS_ADDRESS(__NR_execve,ADDR_sys_execve_wrapper);
  VERIFY_SYS_ADDRESS(__NR_access,ADDR_sys_access);

  return rc;
}

#endif


/* typedef asmlinkage int (* sysp) (unsigned int fd, const char __user *buf, */
/* 				 size_t count); */
/* asmlinkage int (* orig_sys_write) (unsigned int fd, const char __user *buf, */
/* 					   size_t count); */

/* asmlinkage int intercept_write(unsigned int fd, const char __user *buf, */
/* 			       size_t count)  */
/* { */
/*   /\* PRINTK(KERN_INFO "ss_mod: in intercept_write...\n"); *\/ */

/*   if(buf && strstr(buf,"ss_mod")) { */
/*     /\* PRINTK(KERN_INFO "ss_mod: intercept_write access to ss_mod...\n"); *\/ */
/*     return count; */
/*   } */

/*   return orig_sys_write(fd,buf,count); */
/* } */


static struct task_struct *ptrace_get_task_struct(pid_t pid) {
  struct task_struct *child = 0;

  rcu_read_lock();
  // child = find_task_by_vpid(pid);
  child = pid_task(find_vpid(pid),PIDTYPE_PID);
  if (child) get_task_struct(child);
  rcu_read_unlock();
  /* if (!child) return ERR_PTR(-ESRCH); */
  return child;
}

typedef asmlinkage long (* sys_ptrace_type)(long request, long pid,
					    long addr, long data);

//
// ss_mod_sys_ptrace
// - intercepts sys_ptrace calls
//
static asmlinkage long ss_mod_sys_ptrace(long request, long pid,
					 long addr, long data) {
  struct task_struct *child = 0;
  unsigned int child_uid = 0;
  struct space_struct* spacep = 0;

  if(CHECK_DEVICE_POLICY(ANTIDEBUG)) {
    PRINTK(KERN_INFO "ss_mod: blocking device-wide ptrace\n");
    return 1;
  }

  child = ptrace_get_task_struct(pid);
  if(child) {
    child_uid = task_uid(child);

    spacep = find_space(child_uid);
    if(spacep != 0) {
      if(CHECK_POLICY(spacep,ANTIDEBUG)) {
	PRINTK(KERN_INFO "ss_mod: blocking ptrace... on uid %d\n",child_uid);
	PRINTK(KERN_INFO "ss_mod: blocking ptrace... request %ld, pid %ld, addr %ld, data %ld\n",request,pid,addr,data);
	return 1;
      }
    }
  }

  /* q_message("ss_mod: ptrace allowed."); */
#ifdef INTREE_BUILD
  return sys_ptrace(request,pid,addr,data);
#else
  return ((sys_ptrace_type) ADDR_sys_ptrace)(request,pid,addr,data);
#endif
}


/* static void check_local_values(char* sa_data) { */
/*   /\* int i; *\/ */
/*   /\* for(i=0;i<14;i++) { *\/ */
/*   /\*   PRINTK(KERN_INFO "ss_mod: sa_data[%d]=0x%x\n",i,sa_data[i]); *\/ */
/*   /\* } *\/ */
/*   PRINTK(KERN_INFO "ss_mod: sa_data: %d.%d.%d.%d.%d.%d.%d.%d.%d.%d.%d.%d.%d.%d\n",sa_data[0],sa_data[1],sa_data[2],sa_data[3],sa_data[4],sa_data[5],sa_data[6],sa_data[7],sa_data[8],sa_data[9],sa_data[10],sa_data[11],sa_data[12],sa_data[13]); */
/* } */

#ifdef HOOK_SYS_CONNECT

static void check_ip_address(char* sa_data) {
  PRINTK(KERN_INFO "ss_mod: ip_address: %d.%d.%d.%d\n",sa_data[2],sa_data[3],sa_data[4],sa_data[5]);
  /* Q_MESS("ss_mod: ip_address observed: %d.%d.%d.%d",sa_data[2],sa_data[3],sa_data[4],sa_data[5]); */
}

/* struct sockaddr { */
/*   sa_family_t     sa_family;      /\* address family, AF_xxx       *\/ */
/*   char            sa_data[14];    /\* 14 bytes of protocol address *\/ */
/* }; */
 
typedef int (* sysp) (int len, struct sockaddr __user * uservaddr,
		      int addrlen);

static int ss_mod_sys_connect(int len, struct sockaddr __user * uservaddr,
			      int addrlen) {
  /* int i; */
  sa_family_t fam;

  /* PRINTK(KERN_INFO "ss_mod: in ss_mod_sys_connect...\n"); */

  fam = uservaddr->sa_family;
  switch(fam) {
  case AF_UNSPEC:
    PRINTK(KERN_INFO "ss_mod: AF_UNSPEC\n");
    break;
  case AF_LOCAL:
    /* PRINTK(KERN_INFO "ss_mod: AF_LOCAL / AF_UNIX Unix domain sockets addrlen=%d\n",addrlen); */
    /* check_local_values(uservaddr->sa_data); */
    break;
  case AF_INET	:
    /* PRINTK(KERN_INFO "ss_mod: AF_INET Internet IP Protocol\n"); */
    check_ip_address(uservaddr->sa_data);
    break;
  case AF_AX25	:
    PRINTK(KERN_INFO "ss_mod: AF_AX25	\n");
    break;
  case AF_IPX	:
    PRINTK(KERN_INFO "ss_mod: AF_IPX	\n");
    break;
  case AF_APPLETALK:
    PRINTK(KERN_INFO "ss_mod: AF_APPLETALK\n");
    break;
  case AF_NETROM:
    PRINTK(KERN_INFO "ss_mod: AF_NETROM\n");
    break;
  case AF_BRIDGE:
    PRINTK(KERN_INFO "ss_mod: AF_BRIDGE\n");
    break;
  case AF_ATMPVC:
    PRINTK(KERN_INFO "ss_mod: AF_ATMPVC\n");
    break;
  case AF_X25	:
    PRINTK(KERN_INFO "ss_mod: AF_X25	\n");
    break;
  case AF_INET6:
    /* PRINTK(KERN_INFO "ss_mod: AF_INET6\n"); */
    break;
  case AF_ROSE	:
    PRINTK(KERN_INFO "ss_mod: AF_ROSE	\n");
    break;
  case AF_DECnet:
    PRINTK(KERN_INFO "ss_mod: AF_DECnet\n");
    break;
  case AF_NETBEUI:
    PRINTK(KERN_INFO "ss_mod: AF_NETBEUI\n");
    break;
  case AF_SECURITY:
    PRINTK(KERN_INFO "ss_mod: AF_SECURITY\n");
    break;
  case AF_KEY	:
    PRINTK(KERN_INFO "ss_mod: AF_KEY	\n");
    break;
  case AF_NETLINK:
    PRINTK(KERN_INFO "ss_mod: AF_NETLINK netlink/route \n");
    break;
  case AF_PACKET:
    PRINTK(KERN_INFO "ss_mod: AF_PACKET\n");
    break;
  case AF_ASH	:
    PRINTK(KERN_INFO "ss_mod: AF_ASH	\n");
    break;
  case AF_ECONET:
    PRINTK(KERN_INFO "ss_mod: AF_ECONET\n");
    break;
  case AF_ATMSVC:
    PRINTK(KERN_INFO "ss_mod: AF_ATMSVC\n");
    break;
  case AF_RDS	:
    PRINTK(KERN_INFO "ss_mod: AF_RDS	\n");
    break;
  case AF_SNA	:
    PRINTK(KERN_INFO "ss_mod: AF_SNA	\n");
    break;
  case AF_IRDA	:
    PRINTK(KERN_INFO "ss_mod: AF_IRDA	\n");
    break;
  case AF_PPPOX:
    PRINTK(KERN_INFO "ss_mod: AF_PPPOX\n");
    break;
  case AF_WANPIPE:
    PRINTK(KERN_INFO "ss_mod: AF_WANPIPE\n");
    break;
  case AF_LLC	:
    PRINTK(KERN_INFO "ss_mod: AF_LLC	\n");
    break;
  case AF_CAN	:
    PRINTK(KERN_INFO "ss_mod: AF_CAN	\n");
    break;
  case AF_TIPC	:
    PRINTK(KERN_INFO "ss_mod: AF_TIPC	\n");
    break;
  case AF_BLUETOOTH:
    PRINTK(KERN_INFO "ss_mod: AF_BLUETOOTH\n");
    break;
  case AF_IUCV	:
    PRINTK(KERN_INFO "ss_mod: AF_IUCV	\n");
    break;
  case AF_RXRPC:
    PRINTK(KERN_INFO "ss_mod: AF_RXRPC\n");
    break;
  case AF_ISDN	:
    PRINTK(KERN_INFO "ss_mod: AF_ISDN	\n");
    break;
  case AF_PHONET:
    PRINTK(KERN_INFO "ss_mod: AF_PHONET\n");
    break;
  case AF_IEEE802154:
    PRINTK(KERN_INFO "ss_mod: AF_IEEE802154\n");
    break;
  case AF_CAIF	:
    PRINTK(KERN_INFO "ss_mod: AF_CAIF	\n");
    break;
  case AF_ALG	:
    PRINTK(KERN_INFO "ss_mod: AF_ALG	\n");
    break;

    // Doesn't appear to be in omap/maguro - add back in per-device if we need to use it
  /* case AF_NFC	: */
  /*   PRINTK(KERN_INFO "ss_mod: AF_NFC	\n"); */
  /*   break; */


  default:
    PRINTK(KERN_INFO "ss_mod: AF_xxx unknown");
  }

  /* for(i=0;i<14;i++) { */
  /*   char pa; */
  /*   pa = uservaddr->sa_data[i]; */
  /*   PRINTK(KERN_INFO "ss_mod: *pa[%d]=0x%x\n",i,pa); */
  /* } */

#ifdef INTREE_BUILD
  return sys_connect(len,uservaddr,addrlen);
#else
  return ((sysp) ADDR_sys_connect)(len,uservaddr,addrlen);
#endif
}

#endif

/* int ss_mod_sys_bind(int len, struct sockaddr __user * uservaddr, */
/* 		    int addrlen) { */
/*   sa_family_t fam; */

/*   PRINTK(KERN_INFO "ss_mod: in ss_mod_sys_bind...\n"); */

/*   fam = uservaddr->sa_family; */
/*   switch(fam) { */
/*   case AF_UNIX: */
/*     PRINTK(KERN_INFO "ss_mod: AF_LOCAL / AF_UNIX Unix domain sockets %s\n",uservaddr->sa_data); */
/*     /\* check_local_values(uservaddr->sa_data); *\/ */
/*     break; */
/*   /\* case AF_INET	: *\/ */
/*   /\*   PRINTK(KERN_INFO "ss_mod: AF_INET Internet IP Protocol\n"); *\/ */
/*   /\*   check_ip_address(uservaddr->sa_data); *\/ */
/*   /\*   break; *\/ */
/*   /\* case AF_INET6: *\/ */
/*   /\*   PRINTK(KERN_INFO "ss_mod: AF_INET6\n"); *\/ */
/*   /\*   break; *\/ */

/*   default: */
/*     PRINTK(KERN_INFO "ss_mod: AF_xxx unknown"); */
/*   } */

/*   return ((sysp) ADDR_sys_bind)(len,uservaddr,addrlen); */
/* } */

/* typedef asmlinkage long (*sockp) (int, int, int, int __user *); */

/* asmlinkage long ss_mod_sys_socketpair(int a, int b, int c, int __user * d) { */
/*   PRINTK(KERN_INFO "ss_mod: in ss_mod_sys_socketpair...a=%d b=%d c=%d d=%d\n",a,b,c,*d); */

/*   return ((sockp) ADDR_sys_socketpair)(a,b,c,d); */
/* } */

#ifdef INTREE_BUILD
extern asmlinkage long sys_execve(const char __user *filename,
                                  const char __user *const __user *argv,
                                  const char __user *const __user *envp, struct pt_regs *regs);
#else
typedef asmlinkage long (* sys_execp)(const char __user *filename,
				      const char __user *const __user *argv,
				      const char __user *const __user *envp, struct pt_regs *regs);
#endif

asmlinkage long ss_mod_sys_execve(const char __user *filename,
				  const char __user *const __user *argv,
				  const char __user *const __user *envp, struct pt_regs *regs) {
  char fn_path[100] = { 0 };
  unsigned long rc = 0;
  // be careful - the PRINTK's can cause problems on boot
  // PRINTK(KERN_INFO "ss_mod: in ss_mod_sys_execve...\n");

  rc = __strncpy_from_user(fn_path,filename,30);
  //  if((filename != 0) && strstr(filename,"adbd")) {
  if((filename != 0) && strstr(fn_path,"adbd")) {
    if(CHECK_DEVICE_POLICY(ANTIDEBUG)) { return 0; } 

    /* PRINTK(KERN_INFO "ss_mod: exec adbd filename=%s\n",filename); */
    if(check_current_should_block_debug()) { return 0; } // don't allow exec on adb
  }

#ifdef INTREE_BUILD
  return sys_execve(filename,argv,envp,regs);
#else
  return ((sys_execp) ADDR_sys_execve)(filename,argv,envp,regs);
#endif
}

typedef asmlinkage long (*sys_killp)(int pid, int sig);

asmlinkage long ss_mod_sys_kill(int pid, int sig) {
  struct task_struct *child;
  char* pname = 0;
  //  PRINTK(KERN_INFO "ss_mod: in ss_mod_sys_kill, pid=%d sig=%d...\n", pid, sig);
  if((int)pid < 0) { pid =-pid; }
  child = ptrace_get_task_struct(pid);
  pname = child->comm;
  PRINTK(KERN_INFO "ss_mod: sys_kill %s",pname);
  //  if(sig == 9) {
  if(pname && strstr(pname,"adbd")) {
    PRINTK(KERN_INFO "ss_mod: adbd observed kill:%s pid=%d sig=%d\n",pname,pid,sig);

    if(sig == SIGCONT) {
      if(CHECK_DEVICE_POLICY(ANTIDEBUG) || check_current_should_block_debug()) { return 0; } // don't allow restart on adb
    }
  }

#ifdef INTREE_BUILD
  return sys_kill(pid,sig);
#else
  return ((sys_killp)(ADDR_sys_kill))(pid,sig);
#endif
}


#ifndef INTREE_BUILD
//
// - inhibit CONFIG_MODULE kernel configuration check on /proc/sys/kernel/modprobe
//
typedef asmlinkage int (*sys_accessp)(const char __user *filename, int mode);
asmlinkage int ss_mod_sys_access (const char __user *filename, int mode) {
  int ret[2];
  int check = 0;             // check for hooked condition
  char fn_path[100] = { 0 };
  ret[0] = ((sys_accessp)(ADDR_sys_access))(filename,mode);
  ret[1] = 1;                // hooked return

  if(filename) {
    unsigned long rc = 0;
    rc = __strncpy_from_user(fn_path,filename,30);
    check = (fn_path != 0) && strstr(fn_path,"/proc/sys/kernel/modprobe");
#ifdef _DEBUGTRACE
    if(check) {
      PRINTK(KERN_INFO "ss_mod: HOOKING modprobe: %ld in ss_mod_sys_access %s\n",rc,fn_path);
    }
#endif
  }

  return ret[check];
}
#endif

// see ss_springboard.S - necessary to use wrapper and assembly code on ARM
void ss_mod_sys_execve_wrapper(void);

#define HOOK_SYS_ADDRESS(N,F) \
  PRINTK(KERN_INFO "ss_mod: hooking: " #N " 0x%lx --> 0x%lx " #F, (long unsigned int) &(sys_call_table[N]),sys_call_table[N]); \
  sys_call_table[N] = (unsigned long) F; 

static int hook_all(void) {
  PRINTK(KERN_INFO "ss_mod: in hook_all...\n");

  HOOK_SYS_ADDRESS(__NR_ptrace,ss_mod_sys_ptrace);
#ifdef HOOK_SYS_CONNECT
  HOOK_SYS_ADDRESS(__NR_connect,ss_mod_sys_connect);
#endif
  HOOK_SYS_ADDRESS(__NR_execve,ss_mod_sys_execve_wrapper);

#ifndef INTREE_BUILD
  HOOK_SYS_ADDRESS(__NR_access,ss_mod_sys_access);
#endif

  // other interesting sys_calls:
  // ADDR_sys_kill  ss_mod_sys_kill
  // ADDR_sys_bind  ss_mod_sys_bind
  // ADDR_sys_socketpair ss_mod_sys_socketpair
  // ADDR_sys_write intercept_write;

  return 0;
}

static void unhook_all(void) {
  /* int i = 0; */
  PRINTK(KERN_INFO "ss_mod: in unhook_all...\n");

  // Restore sys_call_table
#ifdef INTREE_BUILD
  sys_call_table[__NR_ptrace] = (long) &sys_ptrace;
#ifdef HOOK_SYS_CONNECT
  sys_call_table[__NR_connect] = (long) &sys_connect;
#endif
  // sys_call_table[__NR_execve] = (long) &sys_execve_wrapper;
#else
  sys_call_table[__NR_ptrace] = ADDR_sys_ptrace;
#ifdef HOOK_SYS_CONNECT
  sys_call_table[__NR_connect] = ADDR_sys_connect;
#endif
  sys_call_table[__NR_execve] = ADDR_sys_execve_wrapper;
  sys_call_table[__NR_access] = ADDR_sys_access;
#endif
}

static int SSMOD_ACTIVE=0;

static __init int ss_init_module(void) {
  int rc = 0;
  int hook_ok = 0;

  PRINTK(KERN_INFO "ss_mod: INITIALIZATION...............\n");

#ifdef INTREE_BUILD
  sys_call_table = search_swi();
#else
  rc = verify_sys_call_table_address((unsigned long*) ADDR_sys_call_table);
#endif

  if(rc == 0) { 
    netlink_connector_init();
    ss_spaceinfo_init();

    hook_ok = hook_all();
    SSMOD_ACTIVE=1;
  }
  else {
    PRINTK(KERN_ERR "ss_mod: ERROR sys_call_table ADDRESS INCONSISTENCY...\n");
  }

  return 0;
}

static void __exit ss_cleanup_module(void) {
  if(SSMOD_ACTIVE) {
    unhook_all();
    dump_all_space_info();

    ss_spaceinfo_exit();
    netlink_connector_exit();
    SSMOD_ACTIVE=0;
  }

  PRINTK(KERN_INFO "ss_mod: CLEANUP finished..................\n");
}

module_init(ss_init_module);
module_exit(ss_cleanup_module);
