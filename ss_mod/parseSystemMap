#!/usr/bin/perl -w

#
# parseSystemMap
# - parse a kernel System.map and create a header file
# 
# C.Liem 2013
#

my $PREFIX = "ADDR_";
my $LENGTHPREFIX = "LENGTH_PAST_ADDR_";
my $HOOKPREFIX = "SS_HOOK_";

#
# 1 = create a C variable with $PREFIX . name, initialized with the address
# 2 = same as 1, but also calculate a 2nd variable with $LENGTHPREFIX . $name, initialized to the size based on
#     next symbol found incrementally past the 1st variable
#
my $HOOKED_SYMS = {
    'sys_call_table' => 2,
    'sys_ptrace' => 1,
    'sys_init_module' => 1,
    'sys_delete_module' => 1,
    'sys_write' => 1,
    'sys_socketpair' => 1,
    'sys_connect' => 1,
    'sys_bind' => 1,
    'sys_kill' => 1,
    'sys_execve_wrapper' => 1,
    'sys_execve' => 1,
    'sys_fork_wrapper' => 1,
    'sys_access' => 1,
};

my $inputFile = '';
my $outputFile = "auto_symbol_header.h";

if(scalar(@ARGV) != 0) {
    $inputFile = $ARGV[0];
}

if(scalar(@ARGV) > 1) {
    $outputFile = $ARGV[1];
}

&parseFile($inputFile,$outputFile);

exit(0);

sub parseFile {
    my ($file,$outFile) = @_;
    my %allowHash;
    my %contextHash;

    my $stream;
    if($file ne '') {
	print("...Parsing input file: '$file'\n\n");
	open(FILE,"<$file") || die("Cannot open input file: $file.\n");
	$stream = FILE;
    }
    else {
	$stream = STDIN;
    }

    open(OUT,">$outFile") || die("Cannot open output file: $outFile.\n");
    &printHeader(OUT);

    my $prevAddress = 0;
    my $globalAddress = 0;
    my $distanceFromPrevious = 0;

    while(<$stream>) {
	if(/([0-9a-fA-F]+)\s+([Tt])\s+(\w+)/) {
	    my ($address,$t,$sym) = ($1,$2,$3);
	    my $decAddress = hex($address);

	    # uppercase T - globals only
	    if($t eq 'T') {
		$globalAddress = $decAddress;
		$distanceFromPrevious = $globalAddress - $prevAddress;
	    }

	    # print "DecAddress:$decAddress DistanceFromPrevious:$distanceFromPrevious\n";
	    my $valSet = $HOOKED_SYMS->{$sym};

	    if($valSet) {
		# Print the full definition - symbol address with initialization
		print "$sym = $address\n";
		&printSingleHeaderInfo(OUT,$PREFIX,$sym,$address);
		my $decremented = $valSet - 1;
		$HOOKED_SYMS->{$sym} = $decremented;
		if($decremented == 1) {
		    # Print the length to the next symbol found
		    &printSingleHeaderInfo(OUT,$LENGTHPREFIX,$sym,$distanceFromPrevious);
		}
#		&printNewHookDeclaration(OUT,$HOOKPREFIX,$sym);
		print(OUT "\n");
	    }
	    if($t eq 'T') {
		$prevAddress = $globalAddress;
	    }
	}
    }

    print(OUT "#endif\n");
    close($stream);
    close(OUT);

    print("\n...Wrote output file: '$outFile'\n\n");
}

sub printHeader {
    my ($outstream) = @_;

    print($outstream "#ifndef __AUTO_SYMBOL_HEADER_H\n");
    print($outstream "#define __AUTO_SYMBOL_HEADER_H\n");
    print($outstream "\/*********************************************************\/\n");
    print($outstream "\/*****Automatically generated file - DO NOT MODIFY********\/\n");
    print($outstream "\/*********************************************************\/\n");
    print($outstream "\n");
}

sub printSingleHeaderInfo {
    my ($outstream,$prefix,$sym,$value) = @_;

    print($outstream "const unsigned long ");
    print($outstream "$prefix$sym = 0x$value;\n");
}

sub printNewHookDeclaration {
    my ($outstream,$prefix,$sym) = @_;

    print($outstream "#define SS_HOOK($sym) ");
    print($outstream "$prefix$sym\n");
}


1;
