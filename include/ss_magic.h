#ifndef __SS_MAGIC_H
#define __SS_MAGIC_H

// Defines magic numbers that relay information from userspace to kernel space
// 
#define SS__BLOCKALL  0xFFFFFFFF

#define SS__ANTIDEBUG 0x000000FF
#define SS__PTRACE    0x00000001
#define SS__GDBSERVER 0x00000002
#define SS__ADBD      0x00000004
#define SS__LOGCAT    0x00000008  // not used yet
#define SS__DMESG     0x00000010  // not used yet


#define SS_PUSH_ALL_ANTIDEBUG   SS__ANTIDEBUG
#define SS_PUSH_ALL_NOANTIDEBUG (SS__BLOCKALL ^ SS__ANTIDEBUG)


// DEVICE-wide Space == 9999
#define SS_DEVICEWIDE 0x270F


struct ll {
  unsigned int int1;
  unsigned int int2;
};

#define SS_CMD_MSG               0x01
#define SS_CMD_SPACE_ID          0x02
#define SS_CMD_FOREGROUND_SPACE  0x03
#define SS_CMD_UNREGISTER_SPACE  0x04
#define SS_CMD_PUSH_DEBUG_POLICY 0x05


#endif
